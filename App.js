
import React, { Component } from 'react'
import { View, StyleSheet } from 'react-native';
import { Container, Header, Content, Button, Icon, Text } from 'native-base';
import RNAccountKit from 'react-native-facebook-account-kit';
export default class App extends Component {
  render() {
    RNAccountKit.configure({
      responseType:'code',// 'token' by default,
      initialPhoneCountryPrefix: '+885',
      
      // facebookNotificationsEnabled: true | false, // true by default
      // readPhoneStateEnabled: true | false, // true by default,
      // receiveSMS: true | false, // true by default,
      // countryWhitelist: ['AR'], // [] by default
      // countryBlacklist: ['US'], // [] by default
      defaultCountry: 'CD',
      // theme: { ...}, // for iOS only, see the Theme section
    })




    RNAccountKit.loginWithPhone()
      .then((token) => {
        if (!token) {
          console.log('Login cancelled')
        } else {
          console.log(`Logged with phone. Token: ${JSON.stringify(token)}`)
        }
      })
    return (
      <View style={styles.container}>
        <Text>Welcome with Facebook Account kit you have Login successfully</Text>
        {/* <Button iconLeft style={{ alignSelf: 'center', marginTop: 20 }}>
          <Icon name='person' />
          <Text>Continue with Facebooks</Text>
        </Button> */}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});
